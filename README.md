# [Recommandations pour une mise en pratique des principes de la science ouverte](https://www.ouvrirlascience.fr/wp-content/uploads/2022/02/Academie-des-sciences_Recommandations-science-ouverte_2022-01-27.pdf)

# [La feuille de route 2021-2024 du MESRI sur la politique des données, des algorithmes et des codes sources](https://www.enseignementsup-recherche.gouv.fr/cid159462/www.enseignementsup-recherche.gouv.fr/cid159462/la-feuille-de-route-2021-2024-du-mesri-sur-la-politique-des-donnees-des-algorithmes-et-des-codes-sources.html)

# [Deuxième Plan national pour la science ouverte](https://www.ouvrirlascience.fr/deuxieme-plan-national-pour-la-science-ouverte/) Généraliser la science ouverte en France 2021-2024

Une citation au choix (après une première lecture rapide):
> L’obligation d’ouverture des données de la recherche publique, posée par la loi pour une République numérique de 2016 doit désormais se traduire dans les pratiques scientifiques grâce à des infrastructures et des services d’accompagnement adaptés. Elle est limitée par les exceptions légitimes encadrées par la loi, par exemple en ce qui concerne le secret professionnel, les secrets industriels et commerciaux, les données personnelles ou les contenus protégés par le droit d’auteur. Dans ces cas, les pratiques de partage des données devront être favorisées à travers la définition de protocoles maîtrisés.

Ce document est une mine d'or... La brochure est [ici](https://cache.media.enseignementsup-recherche.gouv.fr/file/science_ouverte/20/9/MEN_brochure_PNSO_web_1415209.pdf) 

# [Baromètre français de la Science Ouverte](https://ministeresuprecherche.github.io/bso/) 


# All FAIR and Open Science-related ressources

**_Attention : cette liste n'est pour l'instant qu'un ramassis de liens. Elle a besoin de curation et de commentaires, quand quelqu'on aura le temps. Mais il faut bien commencer quelque part...._**

**Aide au PGD en France**

- [SOS PGD](https://scienceouverte.couperin.org/sos-pgd/)

**Arbres de décision ouverture des données**

- [Cirad](https://coop-ist.cirad.fr/gerer-des-donnees/diffuser-les-donnees/testez-l-arbre-aide-a-la-decision-sur-la-diffusion-des-donnees-de-recherche)
- [INRAe](https://datapartage.inrae.fr/content/download/3749/39736/version/2/file/LogigrammePPMD.pdf)
- [Pasteur](https://www.pasteur.fr/fr/file/20707/download)
- [Ponts et Chaussées](https://espacechercheurs.enpc.fr/sites/default/files/logigramme_a_plat.pdf)
- [Ouvrir la Science](https://www.ouvrirlascience.fr/wp-content/uploads/2020/10/JepublieQuelssontmesdroits-FLYER-A4.pdf)

**Cours gestion des données et science ouverte**

- [Data Steward Training by EOSC Synergy](https://moodle.learn.eosc-synergy.eu/course/view.php?id=132&section=0#tabs-tree-start)
- [Oberred: a Doranum MOOC](https://www.inist.fr/nos-actualites/oberred-un-nouveau-mooc-sur-les-donnees-de-la-recherche/)
- [S'initier à la Data Science et à ses enjeux ](https://www.fun-mooc.fr/fr/cours/sinitier-la-data-science-et-ses-enjeux/)
- [ELIXIR-SI e-learning platform](https://elixir.mf.uni-lj.si/)
- [Data Tree](https://datatree.org.uk/?redirect=0)
- [Université de Lorraine](http://bu.univ-lorraine.fr/services/domptez-la-doc)
- [Recherche reproductible : principes méthodologiques pour une science transparente](https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about)
- [FAIR data stewardship (for the life sciences) course](https://www.aanmelder.nl/fair-data-stewardship-2021)


**Data management**
- [FAIR Cookbook](https://fairplus.github.io/the-fair-cookbook/content/home.html)
- [ELXIR-CONVERGE RDMkit](https://elixir-europe.org/news/elixir-converge-rdmkit)
- [Data management tips](https://twitter.com/KiraHoeffler/status/1367804034413920259)

**Data stewardship**

- [Competency Hub](https://competency.ebi.ac.uk/framework/datasteward/1.0)
- [Project F: Professionalising data stewardship: competences, training and education](https://www.openscience.nl/en/projects/project-f-professionalising-data-stewardship-competences-training-and-education)

**DMPs/PGDs**

- [ARGOS](https://journeeopenaire.sciencesconf.org/data/pages/Portfolio_Publish_ARGOS_French_NOAD_event_20210702.pdf)
- [DMP Competition Winners](https://blog.dmptool.org/2021/05/19/dmp-competition-winners-dmps-so-good-they-go-to-11/)
- [A collection of 841 publicly available Horizon 2020 Data Management Plans](https://www.openaire.eu/blogs/establishing-a-collection-of-841-horizon-2020-data-management-plans)
- [DMP Assistant](https://assistant.portagenetwork.ca/)


**(Almost) Everything around FAIR**

- Doranum -- Les principes FAIR [ici](https://view.genial.ly/5d64fbbd8352350fa3d22603/interactive-content-les-principes-fair) ou [là](https://doranum.fr/enjeux-benefices/principes-fair/)


**Elixir Core Interoperability Resources**

- always worth a browse: https://elixir-europe.org/platforms/interoperability/rirs

**Entrepôts**

- [17th RDA Plenary Poster Presentation: re3data - Discovering FAIR enabling repositories](https://zenodo.org/record/4705209), voted best poster
- [Dataverse Software Guide for CTS Certification](https://dataverse.org/cts-guide)
- [Aide au choix d’un entrepot](http://busec2.u-bordeaux.fr/aide-choix-entrepot/#général)
- [Livre blanc Approches contemporaines en hébergement et gestion de données](https://lipn.univ-paris13.fr/~cerin/Livre_blanc_data_hosting.pdf)


**ELN matters**

- [Cahiers de laboratoire électronique : Un guide pour vous aider à en choisir un](https://www.ouvrirlascience.fr/cahiers-de-laboratoire-electronique-un-guide-pour-vous-aider-a-en-choisir-un/)
- [Considerations for implementing electronic laboratory notebooks in an academic research environment](https://www.nature.com/articles/s41596-021-00645-8#Tab2)

**FAIR Metrics**

- [F-UJI Automated FAIR Data Assessment Tool](https://www.fairsfair.eu/f-uji-automated-fair-data-assessment-tool): a programmatic assessment of the FAIRness of research datasets in five trustworthy data repositories
- [FAIR Aware](https://fairaware.dans.knaw.nl/): basic questionnaire to help in FAIRification
- [Recommendations on FAIR metrics for EOSC](https://op.europa.eu/en/publication-detail/-/publication/ced147c9-53c0-11eb-b59f-01aa75ed71a1/language-en)
- [FAIRsFAIR Data Object Assessment Metrics](https://fairsfair.eu/fairsfair-data-object-assessment-metrics-request-comments)
- [Fair Checker (developpé par Thomas Rosnet, IFB)](https://fair-checker.france-bioinformatique.fr/base_metrics)
- [RDA: Survey on bridging the gap between funders and communities –perspectives on benefits and challenges of FAIR assessments V1.0](https://www.rd-alliance.org/system/files/20201113_FAIR_survey_v0.07.pdf)

**Formation/training -- Reports, etc...**

- [ELIXIR: Identifying gaps in current training](https://docs.google.com/forms/d/e/1FAIpQLSca-40-pxZIhgEL18bgqOnPYtsnn-7gNOq6d7QIwbFnUh7y-g/viewform)
- [EOSC RDM Training and support catalogue](https://www.eosc-pillar.eu/rdm-training-and-support-catalogue)
- [Former les professionnels de l'information et de la documentation aux données de la recherche en 45 minutes](https://zenodo.org/record/4610514#.YFL-pdzjI2y)

**Guides**

- [awesome-data-steward-resources](https://github.com/Nazeeefa/awesome-data-steward-resources)
- [Comment monter un service d'accompagnement aux données ?](https://zenodo.org/record/4890410)
- [Science Europe: Practical Guide to the International Alignment of Research Data Management](https://www.scienceeurope.org/our-resources/practical-guide-to-the-international-alignment-of-research-data-management/)
- [Guide de bonnes pratiques sur la gestion des données de la recherche](https://mi-gt-donnees.pages.math.unistra.fr/guide/00-introduction.html)
- [Accompagnement à la gestion et à la valorisation des données de la recherche (portail et services)](https://www.oca.eu/fr/rech-lagrange/theses-en-cours/256-categorie-fr-fr/oca/bibfr-principal/1396-gerer-et-diffuser-les-donnees-de-la-recherche)
- [Rapport Bothorel sur la politique publique de la donnée, des algorithmes et des codes sources](https://www.gouvernement.fr/remise-du-rapport-sur-la-politique-publique-de-la-donnee-des-algorithmes-et-des-codes-sources)
- [Des contrats pour la science ouverte](https://www.ouvrirlascience.fr/des-contrats-pour-la-science-ouverte/)
- [Passeport pour la Science Ouverte | Guide pratique à l’usage des doctorants](https://www.ouvrirlascience.fr/passeport-pour-la-science-ouverte-guide-pratique-a-lusage-des-doctorants/)


**Guides juridiques**

- [Guide RGPD](https://www.inshs.cnrs.fr/sites/institut_inshs/files/pdf/Guide_rgpd_2021.pdf)
- [OCDE : Recommandation du Conseil concernant l'accès aux données de la recherche financée sur fonds publics](https://legalinstruments.oecd.org/fr/instruments/OECD-LEGAL-0347)
- [Ouverture des données de la recherche. Guide d'analyse du cadre juridique en France](https://hal.inrae.fr/hal-02791224/file/Guide_Juridique_V1_2.pdf)
- [En route vers la science ouverte : Gérer les données de sa recherche](https://osf.io/8dgfj/)
- [Espace chercheurs : Contexte juridique](https://espacechercheurs.enpc.fr/fr/node/1095)
- [À qui appartiennent les données ?](https://oaamu.hypotheses.org/2513)
- [Les sciences humaines et sociales et la protection des données à caractère personnel dans le contexte de la science ouverte – V2](https://www.inshs.cnrs.fr/sites/institut_inshs/files/pdf/Guide_rgpd_2021.pdf)

**Autres ressources juridiques : PGD**

- [Décret relatif à l'obligation du PGD](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT00004441136) ; daté du 03/12/21, voir article 6

**Autres ressources juridiques : les licences**

- [Décret relatif aux licences de réutilisation à titre gratuit des informations publiques et aux modalités de leur homologation](https://www.etalab.gouv.fr/licence-version-2-0-de-la-licence-ouverte-suite-a-la-consultation-et-presentation-du-decret)
- [Liste des licences autorisées](https://www.data.gouv.fr/en/pages/legal/licences/)
- [Que sont les licences CC- : très éclairant](https://creativecommons.org/faq/#how-do-cc-licenses-operate)
- [Pourquoi diffuser des travaux de recherche sous licence « Pas de modification » n’est pas une bonne idée](https://scinfolex.com/2020/04/27/pourquoi-diffuser-des-travaux-de-recherche-sous-licence-pas-de-modification-nest-pas-une-bonne-idee/)
- [Virevoltantes valses de licences libres et non libres dans les bases de données](https://linuxfr.org/news/virevoltantes-valses-de-licences-libres-et-non-libres-dans-les-bases-de-donnees)



**Integrated RDMS (Research Data Management System)**

- [Kadi4Mat: A Research Data Infrastructure for Materials Science](https://datascience.codata.org/articles/10.5334/dsj-2021-008/)

**Métadonnées etc**

- [Doranum -- Les schémas de métadonnées](https://youtu.be/S-Hw_04ojCc)
- [https://academic.oup.com/gigascience/article/9/12/giaa144/6034785](Making experimental data tables in the life sciences more FAIR: a pragmatic approach)
- [https://inrae.github.io/ODAM/about/](https://inrae.github.io/ODAM/about/)
- [Retour d'expérience "Diffuser librement les métadonnées de la recherche avec Vivo (REX de l'EHESS et de l'UQAM)"](http://devlog.cnrs.fr/jdev2020/t5.p20210218)
- [Citer un jeu de données scientifiques](https://coop-ist.cirad.fr/gerer-des-donnees/citer-un-jeu-de-donnees/1-comprendre-l-interet-de-publier-et-de-citer-un-jeu-de-donnees-scientifiques)

**Reproductibilité : un focus sur**

- [FAIR_Bioinfo: a turnkey training course and protocol for reproducible computational biology](https://jose.theoj.org/papers/10.21105/jose.00068)
- [Journée de travail sur la reproductibilité](https://www.societe-informatique-de-france.fr/journee-reproductibilite/)

**Ressources FAIR organisationnelles**

- [INRAe](https://datapartage.inrae.fr/)

**Retours d'expérience**

- [Université de Lorraine, pdf](https://streaming-canal-u.fmsh.fr/vod/media/canalu/documents/renatis/atelier.dialogu.ist.10.la.science.ouverte.de.la.politique.l.operationnel.definir.une.politique.de.support.aux.donnees.de.la.recherche.l.exemple.de.l.universite.de.lorraine.par.laetitia.bracco.et.thomas.jouneau._59615/20210212_ul.pdf), ou [video](https://www.canal-u.tv/video/renatis/atelier_dialogu_ist_10_la_science_ouverte_de_la_politique_a_l_operationnel_definir_une_politique_de_support_aux_donnees_de_la_recherche_l_exemple_de_l_universite_de_lorraine.59615) : Définir une politique de support aux données de la recherche 

**Revues**

- [Lancement du service Mir@bel de déclaration des politiques de diffusion d’articles en accès ouvert](https://www.ouvrirlascience.fr/lancement-du-service-mirbel-de-declaration-des-politiques-de-diffusion-darticles-en-acces-ouvert/)

**RGPD et loi Jardé**

Les documents sont de qualité variable, à vous de voir.
- [Excellent, de l'INRAE : le RGPD pas à pas](https://nextcloud.inrae.fr/s/5jT3KH7kpESSo4s)
- [Excellent : La loi Jardé et le RGPD](https://philippeamiel.fr/TableauRIPH-v12_diff.pdf)
- [Paris Créteil](https://www.infectiologie.com/UserFiles/File/formation/desc/2019/seminaire-avril-2019/jeudi-04-04-2019/recherche-9-jeudi-04-dr-gallien.pdf)
- [La loi Jardé en 4 minutes](https://soepidemio.com/2020/01/22/la-loi-jarde-en-4-minutes/)
- [Inserm --Comprendre la recherche clinique](https://www.inserm.fr/recherche-inserm/recherche-clinique/comprendre-recherche-clinique)
- [RGPD – CNIL - CEREES](https://www.recherchecliniquepariscentre.fr/wp-content/uploads/2019/01/DIU-COMMUN-RGPD-CNIL-CQ-3.pdf)
- [logigramme qui a l'air très bien Jardé-RGDP](https://www.infectiologie.com/UserFiles/File/renarci/reglementaire-recherche-sur-donnees.pdf)
- [Impact du Règlement Général sur la Protection des Données (RGPD) en Recherche Clinique](https://www.oncopaca.org/sites/default/files/slides_impact_du_reglement_general_sur_la_protection_des_donnees_rgpd_en_rc_c.lovera_cal_-_janvier_2019.pdf)
- CNIL-ARS --Université Paris Nanterre, Les recherches impliquant la personne humaine : le document / les diapos sont directement en ligne.


**Science ouverte**

- [Étude de faisabilité d’un service générique d’accueil et de diffusion des données simples : premières études](https://www.ouvrirlascience.fr/etude-de-faisabilite-dun-service-generique-daccueil-et-de-diffusion-des-donnees-simples-premieres-etudes/)
- [Le site couperin de la science ouverte en France](https://scienceouverte.couperin.org/category/donnees/)


**Sémantique**

- [Data dictionary cookbook](https://zenodo.org/record/4683066)
- [Web sémantique et Web de données](https://www.fun-mooc.fr/fr/cours/web-semantique-et-web-de-donnees/)
- [Article: 39 Hints to Facilitate the Use of Semantics for Data on Agricult](https://datascience.codata.org/article/10.5334/dsj-2020-047/)
- [Clearing some of the highest FAIR hurdles: PIDs, Metadata, and Semantic Interoperability for Researchers](https://www.fairsfair.eu/events/clearing-some-highest-fair-hurdles-pids-metadata-and-semantic-interoperability-researchers)

**Significant papers**

- [Comment la science ouverte peut s’inspirer du libre accès aux données publiques](https://theconversation.com/comment-la-science-ouverte-peut-sinspirer-du-libre-acces-aux-donnees-publiques-157091)
- [The FAIR Guiding Principles for scientific data management and stewardship](https://www.nature.com/articles/sdata201618),     Mark D. Wilkinson, Michel Dumontier _et al_, Barend Mons, Scientific Data, volume 3, (2016)

**Solutions/outils**

- [THE tool: RDMKit](https://rdmkit.elixir-europe.org/)
- [bitwarden](https://bitwarden.com/)
- [Length Complexity for passwords](https://imgur.com/gallery/zFyBtyA)
- transfert de fichiers https://www.globus.org/

**SPARCL --endpoints, resources**

- [List of datasets with a SPARQL endpoint](https://io.datascience-paris-saclay.fr/query/List_of_datasets_with_a_SPARQL_endpoint)
- [SPARQL Endpoints Status > Datahub.io](https://sparqles.ai.wu.ac.at/)
- [Monitoring the Status of SPARQL Endpoints](https://users.dcc.uchile.cl/~ahogan/docs/iswc_demo_sparql.pdf)


**Thésaurus**

- [LOTERRE](https://www.loterre.fr/) : Loterre (Linked open terminology resources) est une plateforme d’exposition et de partage de terminologies scientifiques multidisciplinaires et multilingues, conforme aux standards du web des données ouvertes et liées (LOD) ainsi qu’aux principes FAIR.

**Web annotation**

- [hypothesis](https://web.hypothes.is/)
- Semaphora: work in progress, no portal yet
- [Bioschemas markup generator](http://www.macs.hw.ac.uk/SWeL/BioschemasGenerator/)

**Why Open Science?**

- [Commission publishes study on the impact of Open Source on the European economy](https://digital-strategy.ec.europa.eu/en/news/commission-publishes-study-impact-open-source-european-economy?pk_source=twitter&pk_medium=social_media_organic&pk_campaign=study_on_the_impact_of_open_source_on_the_european_economy&pk_content=open_source)
- [Understanding Open Data](https://think.f1000research.com/open-data/?gclid=CjwKCAjwndCKBhAkEiwAgSDKQQyuRbqjocn0rFe_ACtHhQyzbxbqzuieWH-oFHhodjoT1iDnrlzbVRoCggUQAvD_BwE)

